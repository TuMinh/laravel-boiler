<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'Laravel');

// Project repository
set('repository', 'git@gitlab.com:techchainvn/laravel-boilerplate.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

set('bin/npm', function () {
    return run('which npm');
});

// Shared files/dirs between deploys
add('shared_files', [
    '.env',
    'storage/logs'
]);
add('shared_dirs', ['storage']);

// Writable dirs by web server
add('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/logs',
]);


// Hosts
// Host server develop
host('host.dev')
    ->user('ubuntu')
    ->stage('dev')
    ->identityFile('~/.ssh/id_rsa')
    ->set('branch', 'dev')
    ->set('deploy_path', '/var/www/html/{{application}}');

// Host server staging
host('host.staging')
    ->user('ubuntu')
    ->stage('staging')
    ->identityFile('~/.ssh/id_rsa')
    ->set('branch', 'dev')
    ->set('deploy_path', '/var/www/html/{{application}}');

// Host server product
host('host.product')
    ->user('ubuntu')
    ->stage('product')
    ->identityFile('~/.ssh/id_rsa')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/html/{{application}}');

// Tasks
// Build
task('build', function () {
    run('cd {{release_path}} && build');
});

// Reload fpm
task('reload:php-fpm', function () {
    run('sudo /usr/sbin/service php7.2-fpm reload');
});

//Install npm
task('npm:install', function () {
    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/node_modules ]')) {
            run('cp -R {{previous_release}}/node_modules {{release_path}}');
        }
    }

    run('cd {{release_path}} && {{bin/npm}} install');
});

//Build laravel mix
task('npm:run_dev', function () {
    run('cd {{release_path}} && {{bin/npm}} run dev');
});

// Build api doc
task('npm:apidoc', function () {
    run('cd {{release_path}} && {{bin/npm}} run apidoc');
});

task('build:server', [
//    'reload:php-fpm',
//    'artisan:migrate',
    'npm:install',
    'npm:run_dev',
    'npm:apidoc'
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'build:server');
