<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// API version 1.0.0
Route::middleware('api.key')->prefix('/v1/')->group(function () {
    require __DIR__.'/api/version_1.0.0/api.php';
});
