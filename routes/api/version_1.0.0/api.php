<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
Route::namespace('API\V1')->middleware('api')->group(function () {
    Route::prefix('/users')->group(function () {
        Route::get('/{id}', 'UserController@getDetail')->where('id', '[0-9]+');
        Route::post('/', 'UserController@register');
        Route::put('/{id}', 'UserController@update')
            ->where('id', '[0-9]+')
            ->middleware('authenticate.api');
        Route::post('/login', 'UserController@login');

        Route::prefix('/password')->group(function () {
            Route::post('/forgot-password', 'UserController@forgotPassword');
            Route::put('/recover-password', 'UserController@recoverPassword');
            Route::put('/reset-password', 'UserController@resetPassword')->middleware('authenticate.api');
        });
    });

    Route::prefix('/images')->group(function () {
        Route::post('/', 'PictureController@upload');
        Route::delete('/{id}', 'PictureController@delete');
    });
});
