<?php

namespace App\Models;

use App\Models\Traits\Pictures\PictureUploadTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Picture extends Model
{
    use SoftDeletes;
    use PictureUploadTrait;

    protected $table = 'pictures';
    protected $fillable = [
        'user_id',
        'path',
        'bucket',
        'url'
    ];
}
