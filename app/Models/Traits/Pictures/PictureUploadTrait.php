<?php

namespace App\Models\Traits\Pictures;

use Illuminate\Support\Facades\Storage;

trait PictureUploadTrait
{
    public function uploadToS3($file, $path = null) {
        return $this->handleUploadToS3($file, $path);
    }

    /**
     * @param $file
     * @param $path
     * @return string
     */
    public function handleUploadToS3($file, $path = null) {
        $imageUploadUrl = '';
        $filePath = definePathFileUpload($file, $path);
        $fileUpload = Storage::disk('s3')->put($filePath, file_get_contents($file));

        if ($fileUpload) {
            $imageUploadUrl = env('AWS_URL').'/'.$filePath;
        }

        return $imageUploadUrl;
    }
}
