<?php

namespace App\Models\Traits\User\JWT;

use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Token;

trait JWTUserTrait
{
    /**
     * Generate token for User
     *
     * @return array
     */
    public function generateToken() {
        $token = $this->generateTokenByTime(config('jwt.ttl'));
        $refreshToken = $this->generateTokenByTime(config('jwt.refresh_ttl'));

        return [
            'token' => $token,
            'refresh_token' => $refreshToken
        ];
    }

    /**
     * Generate token for User
     *
     * @param null $exp
     * @return mixed
     */
    private function generateTokenByTime($exp = null) {
        $customClaims = [
            'sub' => $this->id,
            'id' => $this->id,
            'email' => $this->email
        ];

        if (!empty($exp)) {
            $customClaims['exp'] = Carbon::now()->addDays($exp / (24*60));
        }

        $factory = JWTFactory::customClaims($customClaims);
        $payload = $factory->make();
        $token = JWTAuth::encode($payload);

        return array_values((array)$token)[0];
    }

    /**
     * Decode jwt token
     *
     * @param $token
     * @return mixed
     */
    public function decodeToken($token) {
        return JWTAuth::decode(new Token($token));
    }
}
