<?php

namespace App\Repositories\Eloquents;

use App\Models\Picture;
use App\Repositories\Contracts\PictureRepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PictureRepositoryEloquent
 * @package App\Repositories\Eloquents
 */
class PictureRepositoryEloquent extends BaseRepository implements PictureRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Picture::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param $data
     * @return mixed
     */
    public function insert($data)
    {
        return $this->model->instert($data);
    }
}
