<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PictureRepositoryInterface
 * @package App\Repositories\Contract
 */
interface PictureRepositoryInterface extends RepositoryInterface
{
    public function insert($data);
}
