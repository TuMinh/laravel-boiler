<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package App\Repositories\Contract
 */
interface UserRepositoryInterface extends RepositoryInterface
{
    //
}
