<?php
if (! function_exists('buildDataPaginate')) {
    function buildDataPaginate($request)
    {
        $limit = $request->limit ?? config('repository.pagination.limit');
        $page = $request->page ?? 0;

        return [
            'limit' => $limit,
            'page' => $page
        ];
    }
}

if (! function_exists('successStr')) {
    function successStr() {
        return config('common.status.success');
    }
}

if (! function_exists('errorStr')) {
    function errorStr() {
        return config('common.status.error');
    }
}

if (! function_exists('success')) {
    function success($message = '', $data = []) {
        $message = empty($message) ? __('message.common.request.success') : $message;

        return [
            'status' => successStr(),
            'message' => $message,
            'data' => $data
        ];
    }
}

if (! function_exists('error')) {
    function error($message = '', $data = []) {
        $message = empty($message) ? __('message.common.request.error') : $message;

        return [
            'status' => errorStr(),
            'message' => $message,
            'data' => $data
        ];
    }
}



if (! function_exists('isValidMd5')) {
    function isValidMd5($md5 = '') {
        return strlen($md5) == 32 && ctype_xdigit($md5);
    }
}

if (! function_exists('definePathFileUpload')) {
    function definePathFileUpload($file, $path = null) {
        $time = \Carbon\Carbon::now()->timestamp;
        $fileName = $time.'-'.$file->getClientOriginalName();

        if ($file) {
            return empty($path) ? config('common.upload_s3.path').'/'.$fileName : $path.'/'.$fileName;
        }

        return null;
    }
}
