<?php

namespace App\Providers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RepositoryServiceProvider::class);

        Builder::macro('clearOrdersBy', function () {
            $this->{$this->unions ? 'unionOrders' : 'orders'} = null;

            return $this;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Schema::defaultStringLength(191);
        $this->registerResponseMacros();
    }

    /**
     * Macro response
     */
    private function registerResponseMacros()
    {
        Response::macro('success', function (
            $data = [],
            $message = '',
            $statusCode = 200,
            $options = null
        ) {
            $message = empty($message) ? __('message.common.request.success') : $message;

            return response()->json([
                'success'       => true,
                'status_code'   => $statusCode,
                'messages'      => $message,
                'data'          => $data
            ], $statusCode, [], $options);
        });

        Response::macro('error', function (
            $data = [],
            $message = '',
            $statusCode = 400,
            $option = null
        ) {
            $message = empty($message) ? __('message.common.request.error') : $message;

            return response()->json([
                'success'       => false,
                'status_code'   => $statusCode,
                'messages'      => $message,
                'data'          => $data
            ], $statusCode, [], $option);
        });

        Response::macro('authorization', function () {
            return response()->json([
                'success'       => false,
                'status_code'   => 401,
                'messages'      => __('message.error.auth.un_authorization'),
                'data'          => []
            ], 401, []);
        });

        Response::macro('paginate', function (
            $data,
            $limit,
            $page,
            $message = '',
            $statusCode = 200
        ) {
            $message = empty($message) ? __('message.common.request.success') : $message;
            $response = $data;
            $data = [
                'total'         => $response->total(),
                'limit'         => $limit,
                'page'          => $page,
                'max_page'      => $response->lastPage(),
                'orders'        => $response->all()
            ];

            return response()->json([
                'success' => true,
                'status_code' => $statusCode,
                'messages'    => $message,
                'data'        => $data
            ], $statusCode, []);
        });
    }
}
