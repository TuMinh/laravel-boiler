<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\Contracts\UserRepositoryInterface::class,
            \App\Repositories\Eloquents\UserRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\Contracts\PictureRepositoryInterface::class,
            \App\Repositories\Eloquents\PictureRepositoryEloquent::class
        );
    }
}
