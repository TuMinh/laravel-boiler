<?php

namespace App\Services\Users;

use App\Models\Picture;
use App\Repositories\Contracts\PictureRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class PictureService
{
    protected $repository;

    public function __construct(
        PictureRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @param $request
     * @return array
     */
    public function upload($request) {
        $data = $this->uploadToS3AndCreateDataInsert($request);

        return success('', $this->repository->create($data));
    }

    /**
     * @param $id
     * @return array
     */
    public function delete($id) {
        $picture = $this->repository->find($id);

        if (empty($picture)) {
            return error(__('message.error.not_found', ['object' => 'Picture']));
        }

        $picture->delete();
        Storage::disk('s3')->delete($picture->path);

        return success('', $picture);
    }

    /**
     * @param $request
     * @return array
     */
    private function uploadToS3AndCreateDataInsert($request) {
        $data = '';
        $user = $request->user ?? null;
        $file = $request->hasFile('file') ? $request->file('file') : '';

        if ($file) {
            $url = $this->repository->makeModel()->uploadToS3($file);
            if ($url) {
                $data = $this->createDataPicture($file, $user);
            }
        }

        return $data ?? null;
    }

    /**
     * @param $file
     * @param $path
     * @param null $user
     * @return array
     */
    private function createDataPicture($file, $user = null, $path = null) {
        $bucket = env('AWS_URL');
        $time = Carbon::now();
        $filePath = definePathFileUpload($file);

        return [
            'user_id' => empty($user) ? null : $user->id,
            'bucket' => $bucket,
            'path' => $filePath,
            'url' => $bucket.'/'.$path,
            'created_at' => $time,
            'updated_at' => $time
        ];
    }
}
