<?php

namespace App\Services\Users;

use App\Mail\ForgotPassword;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserService
{
    protected $repository;

    public function __construct(
        UserRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @param $request
     * @return array
     */
    public function login($request) {
        $email = $request->email;
        $password = $request->password;
        $token = auth()->attempt(['email' => $email, 'password' => $password]);

        if ($token) {
            $user = auth()->user();
            $this->updateToken($user);

            return success('', $user);
        }

        return error(__('message.error.login'));
    }

    /**
     * @param $user
     * @return mixed
     */
    private function updateToken($user) {
        $token = JWTAuth::fromUser($user);
        $refreshToken = JWTAuth::customClaims([
            'exp' => Carbon::now()->addDays(config('jwt.refresh_ttl')/(24*60))->timestamp,
            'typ' => 'refresh'
        ])->fromUser($user);

        $user->api_token = $token;
        $user->refresh_token = $refreshToken;

        return $user->save();
    }

    /**
     * @param $id
     * @return array
     */
    public function getDetail($id) {
        $user = $this->repository->findByField('id', $id)->first();

        if ($user) {
            return success('', $user);
        }

        return error(__('message.error.not_found', ['object' => 'User']));
    }

    /**
     * @param $request
     * @return array
     */
    public function register($request) {
        DB::beginTransaction();
        try {
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ];
            $user = $this->repository->create($data);
            $this->updateToken($user);

            DB::commit();

            return success('', $user);
        } catch (\Exception $exception) {
            DB::rollback();

            return error($exception->getMessage());
        }
    }

    /**
     * @param $request
     * @param $id
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, $request) {
        $user = $request->user;

        if ($id != $user->id) {
            return error(__('message.error.permission_denied'));
        }

        $data = $request->only(['name']);
        $user = $this->repository->update($data, $user->id);

        return success('', $user);
    }

    /**
     * @param $request
     * @return array
     */
    public function forgotPassword($request) {
        $email = $request->email;
        $user = $this->repository->findByField('email', $email)->first();

        if (!$user) {
            return error(__('message.error.not_found', ['object' => 'Email']));
        }

        $randomEmailCode = strtoupper(Str::random(6));
        Mail::to($user)->queue(new ForgotPassword($randomEmailCode));

        $user->email_code = $randomEmailCode;
        $user->save();

        return success(__('message.check_email.forgot_password'));
    }

    /**
     * @param $request
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function recoverPassword($request) {
        $email = $request->email;
        $code = $request->code;
        $password = $request->password;

        $user = $this->repository->findWhere([
            'email' => $email,
            'email_code' => $code
        ])->first();

        if (!$user) {
            return error(__('message.error.invalid', ['object' => 'Code']));
        }

        $userUpdate = $this->repository->update([
            'password' => Hash::make($password),
            'email_code' => ''
        ], $user->id);

        return success('', $userUpdate);
    }

    /**
     * @param $request
     * @return array
     */
    public function resetPassword($request) {
        $oldPassword = $request->old_password;
        $newPassword = $request->new_password;
        $user = $request->user;

        if (Hash::check($oldPassword, $user->password)) {
            $user->password = Hash::make($newPassword);
            $user->save();

            return success();
        }

        return error(__('message.error.reset_password.wrong_password'));
    }
}
