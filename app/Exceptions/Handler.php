<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @param Exception $exception
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function render($request, Exception $exception)
    {
        if ($request->is('api/*')) {
            if ($exception instanceof ValidationException) {
                return response()->error(
                    $exception->errors(),
                    $this->handleResponseMessageValidate($exception->errors()),
                    $exception->status
                );
            }

            if ($exception instanceof HttpException && $exception->getStatusCode() == 404) {
                return response()->error([], 'API Not found!', 404);
            }

            if ($exception instanceof ModelNotFoundException) {
                return response()->error([], 'Not found!', 404);
            }
        }

        return parent::render($request, $exception);
    }

    /**
     * @param $errors
     * @return mixed
     */
    private function handleResponseMessageValidate($errors) {
        foreach (collect($errors) as $key => $value) {
            return empty($value[0]) ? null : $value[0];
        }
    }
}
