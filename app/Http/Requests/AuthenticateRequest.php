<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class AuthenticateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $bearer = 'Bearer';
        $token = $this->header('authorization');

        if (empty($token)) {
            return false;
        }

        $explodeToken = explode(' ', $token);
        $token = empty($explodeToken[1]) ? null : $explodeToken[1];
        if ($explodeToken[0] != $bearer || empty($token)) {
            return false;
        }

        $this->user = User::where('api_token', $explodeToken[1])->first();

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
