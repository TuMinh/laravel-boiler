<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\AuthenticateRequest;
use App\Rules\IsMd5;

class UserResetPasswordRequest extends AuthenticateRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => ['required', new isMd5],
            'new_password' => ['required', new isMd5],
            're_new_password' => ['required', new isMd5, 'same:new_password'],
        ];
    }
}
