<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\AuthenticateRequest;

class UserUpdateRequest extends AuthenticateRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
        ];
    }
}
