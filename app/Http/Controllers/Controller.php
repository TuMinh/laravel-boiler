<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function isSuccess($response) {
        $status = empty($response['status']) ? '' : $response['status'];

        return $status == successStr();
    }

    public function isError($response) {
        $status = empty($response['status']) ? '' : $response['status'];

        return $status == errorStr();
    }
}
