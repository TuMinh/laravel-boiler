<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UploadFileRequest;
use App\Services\Users\PictureService;

class PictureController extends Controller
{
    protected $service;

    public function __construct(PictureService $service)
    {
        $this->service = $service;
    }

    /**
     * @param UploadFileRequest $request
     * @return mixed
     */
    public function upload(UploadFileRequest $request) {
        $response = $this->service->upload($request);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        $response = $this->service->delete($id);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }
}
