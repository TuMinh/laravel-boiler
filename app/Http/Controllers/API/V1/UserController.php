<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserForgotPasswordRequest;
use App\Http\Requests\Users\UserLoginRequest;
use App\Http\Requests\Users\UserRegisterRequest;
use App\Http\Requests\Users\UserResetPasswordRequest;
use App\Http\Requests\Users\UserUpdateRequest;
use App\Services\Users\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $service;

    public function __construct(
        UserService $service
    ) {
        $this->service = $service;
    }

    /**
     * @apiDefine Header
     * @apiHeader {String} Authorization Authorization = Bearer + token
     */

    /**
     * @api {post} v1/users/login Login
     * @apiName Login
     * @apiGroup Users
     *
     * @apiVersion 1.0.0
     *
     * @apiParam {string} email Email
     * @apiParam {string} password Password must be md5
     */

    /**
     * @param UserLoginRequest $request
     * @return mixed
     */

    public function login(UserLoginRequest $request) {
        $response = $this->service->login($request);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }

    /**
     * @api {get} v1/users/{user-id} Get User information
     * @apiName Get user
     * @apiGroup Users
     *
     * @apiVersion 1.0.0
     */

    /**
     * @param $id
     * @return mixed
     */
    public function getDetail($id) {
        $response = $this->service->getDetail($id);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }

    /**
     * @api {post} v1/users Register user
     * @apiName Register user
     * @apiGroup Users
     *
     *
     * @apiVersion 1.0.0
     *
     * @apiParam {email} email Email user
     * @apiParam {String} [name] Name
     * @apiParam {String} password The password must be md5
     */

    /**
     * @param UserRegisterRequest $request
     * @return mixed
     */
    public function register(UserRegisterRequest $request) {
        $response = $this->service->register($request);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }

    /**
     * @api {put} v1/users/{user-id} Update User information
     * @apiName Update user
     * @apiUse Header
     * @apiGroup Users
     *
     * @apiVersion 1.0.0
     *
     * @apiParam {string} name Name
     */

    /**
     * @param $id
     * @param UserUpdateRequest $request
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update($id, UserUpdateRequest $request) {
        $response = $this->service->update($id, $request);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }

    /**
     * @api {post} v1/users/password/forgot-password Forgot password
     * @apiName Forgot password
     * @apiGroup Users
     *
     * @apiVersion 1.0.0
     *
     * @apiParam {string} email Email
     */

    /**
     * @param UserForgotPasswordRequest $request
     * @return mixed
     */
    public function forgotPassword(UserForgotPasswordRequest $request) {
        $response = $this->service->forgotPassword($request);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data'], $response['message']);
    }

    /**
     * @api {post} v1/users/password/recover-password Recover password
     * @apiName Recover password
     * @apiGroup Users
     *
     * @apiVersion 1.0.0
     *
     * @apiParam {string} email Email
     * @apiParam {string} password Password must be md5
     * @apiParam {string} code Code recover password
     */

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function recoverPassword(Request $request) {
        $response = $this->service->recoverPassword($request);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }

    /**
     * @api {put} v1/users/password/reset-password Reset password
     * @apiName Reset password
     * @apiGroup Users
     *
     * @apiVersion 1.0.0
     *
     * @apiParam {string} old_password Old password must be md5
     * @apiParam {string} new_password New password must be md5
     * @apiParam {string} re_new_password Re new password must be md5
     */

    /**
     * @param UserResetPasswordRequest $request
     * @return mixed
     */
    public function resetPassword(UserResetPasswordRequest $request) {
        $response = $this->service->resetPassword($request);

        if ($this->isError($response)) {
            return response()->error([], $response['message']);
        }

        return response()->success($response['data']);
    }
}
