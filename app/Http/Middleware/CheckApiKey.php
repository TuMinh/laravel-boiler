<?php

namespace App\Http\Middleware;

use Closure;

class CheckApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = $request->header('api_key'); // Api key must be md5

        if (empty($apiKey) || $apiKey != md5(config('common.auth.api_key'))) {
            return response()->error([], __('message.error.permission_denied'), 400);
        }

        return $next($request);
    }
}
