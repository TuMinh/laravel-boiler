<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticateApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bearer = 'Bearer';
        $token = $request->header('authorization');
        $apiKey = $request->header('api_key');

        if (empty($apiKey) || $apiKey != md5(config('common.auth.api_key'))) {
            return response()->error([], __('message.error.permission_denied'), 400);
        }

        if (empty($token)) {
            return response()->error([], __('message.error.auth.un_authorization'), 401);
        }

        $explodeToken = explode(' ', $token);
        $token = empty($explodeToken[1]) ? null : $explodeToken[1];
        if ($explodeToken[0] != $bearer || empty($token)) {
            return response()->error([], __('message.error.auth.token'));
        }

        if (!JWTAuth::parsetoken()->check()) {
            return response()->error([], __('message.error.auth.token_expired'));
        }

        $user = User::where('api_token', $token)->first();

        if (empty($user)) {
            return response()->error([], __('message.error.auth.un_authorization'), 401);
        }

        $request->user = $user;
        $request->token = $user->api_token;

        return $next($request);
    }
}
