const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/backend/backend.js', 'public/js/backend')
    .js('resources/js/frontend/frontend.js', 'public/js/frontend')
    .sass('resources/sass/backend/backend.scss', 'public/css/backend')
    .sass('resources/sass/frontend/frontend.scss', 'public/css/frontend')
    .version();

if (mix.inProduction()) {
    mix.version();
}
