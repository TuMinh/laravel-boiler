## Run composer
```
composer install
```
## Run npm
```
npm install / npm i
```
## Setup check code before commit
* Install git-hooks
```
php artisan git:install-hooks
```
* Run check coding conventions
```
php artisan git:pre-commit
```

## Setup apidoc

* Install apidoc
```
npm install apidoc -g
```

* Command line build apidoc
```
apidoc -i app/Http/Controllers -o public/apidoc
or
npm run apidoc
```

* *Link web page: `{url}/apidoc/`, Document [click here](https://apidocjs.com/)*

## Config JWT
* Generate JWT secret
```
php artisan jwt:secret
```

* Add config time token in file `.env`
```
JWT_REFRESH_TTL = 43200   #minutes = 30 days
JWT_TTL = 10080   #minutes = 7 days
```

## Deployment

* Install `deploy` 
```
curl -LO https://deployer.org/deployer.phar
mv deployer.phar /usr/local/bin/dep
chmod +x /usr/local/bin/dep
```

* Config deployment in file `deploy.php`

* Run `deploy`
```
dep deploy dev 
dep deploy staging
dep deploy product
```

* Show list command
```
dep list
```

* Document [Here](https://deployer.org)
