<?php

return [
    'status' => [
        'success' => 'success',
        'error' => 'error'
    ],
    'auth' => [
        'jwt_secret' => env('JWT_SECRET'),
        'api_key' => env('API_KEY')
    ],
    'upload_s3' => [
        'path' => 'images'
    ]
];
