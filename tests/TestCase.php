<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    const POST = 'POST';
    const GET = 'GET';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    public function customSendRequest($method, $uri, $data, $header = []) {
        return $this->json($method, $uri, $data, array_merge(['api_key' => md5(env('API_KEY'))], $header));
    }
}
