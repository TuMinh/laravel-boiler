<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use PhpParser\Node\Expr\Cast\Object_;
use Tests\TestCase;
use App\Http\Controllers\API\V1\UserController;
use Mockery;

class UserControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $user;
    /**
     * @var Faker
     */
    protected $faker;

    /**
     * setup parameter for test
     */
    public function setUp(): void
    {
        $this->faker = Faker::create();
        parent::setUp();
        $this->user = new User();
        $this->user->email = $this->faker->unique()->safeEmail;
        $this->user->password = Hash::make(md5(12345));
        $this->user->save();
    }

    /**
     * @throws \Throwable
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $this->user = null;
    }

    /**
     * Test login success
     */
    public function testLoginSuccess()
    {
        $response = $this->customSendRequest(self::POST, '/api/v1/users/login', [
            'email' => $this->user->email,
            'password' => md5(12345)
        ]);
        $content = json_decode($response->getContent());
        $response->assertStatus(200);
        $this->assertTrue(is_object($content->data));
    }

    /**
     * Test login error
     */
    public function testLoginError()
    {
        $response = $this->customSendRequest(self::POST, '/api/v1/users/login', [
            'email' => $this->user->email,
            'password' => md5(1234)
        ]);
        $content = json_decode($response->getContent());
        $response->assertStatus(400);
        $this->assertTrue(is_array($content->data));
    }

    /**
     * Test get detail user success
     */
    public function testGetDetailSuccess()
    {
        $response = $this->customSendRequest(self::GET, '/api/v1/users/'.$this->user->id,
            []
        );
        $content = json_decode($response->getContent());

        $response->assertStatus(200);
        $this->assertTrue(is_object($content->data));
    }

    /**
     * Test get detail user success
     */
    public function testGetDetailError()
    {
        $response = $this->customSendRequest(self::GET, '/api/v1/users/'.rand(),
            []
        );
        $content = json_decode($response->getContent());

        $response->assertStatus(400);
    }
}
