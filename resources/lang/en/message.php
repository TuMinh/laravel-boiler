<?php

return [
    'common' => [
        'request' => [
            'success' => 'Request success',
            'error' => 'Request error'
        ]
    ],

    'error' => [
        'invalid' => ':object invalid!',
        'auth' => [
            'un_authorization' => 'Unauthorized!',
            'token' => 'Token invalid!',
            'token_expired' => 'Token has been expired!'
        ],
        'permission_denied' => 'Permission denied!',
        'not_found' => ':object not found!',
        'update' => 'Update failed!',
        'login' => 'Invalid email or password!',
        'reset_password' => [
            'wrong_password' => 'Wrong password!'
        ]
    ],

    'check_email' => [
        'forgot_password' => 'Code has been send to your email!'
    ]
];
