<div style="width: 100%">
    <p style="text-align: center; font-size: 25px">Please use the code below to retrieve the password</p>
</div>
<div style="width: 50%; background-color: #2e6da4; margin: 0 auto">
    <h1 style="color: white; text-align: center; padding: 20px">
        {{ $code }}
    </h1>
</div>
